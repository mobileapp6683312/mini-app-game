import React from "react";
import { Text, StyleSheet, View, Platform} from "react-native";
import Colors from "../../constants/Colors.android";
function Title({ children }) {
  return <Text style={styles.title}>{children}</Text>;
}
export default Title;
const styles = StyleSheet.create({
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 24,
    // fontWeight: 'bold',
    color: "white",
    textAlign: "center",
    borderWidth: 0,
    borderColor: 'white',
    padding: 12,
    maxWidth: '80%',
    // width: '100%'
  },
});
